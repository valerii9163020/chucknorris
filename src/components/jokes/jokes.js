async function getRecource(url) {
  let res = await fetch(url);
  if (!res.ok) {
    throw new Error(`Could not fetch ${url}, status ${res.status}`);
  }
  return await res.json();
}

const jokesCard = document.querySelector(".jokes__cards");
const template = document.querySelector("#template");

const getJoke = document.querySelector("#getJoke");
console.log(getJoke.nextSibling);
let timerID;

const arrQuotesFromLocalstorage = localStorage.getItem("arrQuotes");
let arrQuotes = arrQuotesFromLocalstorage ? JSON.parse(arrQuotesFromLocalstorage) : [];

getJoke.addEventListener("click", () => {
  const spinner = document.querySelector(".spinner");
  spinner.classList.add("spinner_visible");
  if (!timerID) {
    timerID = setInterval(() => {
      getRecource("https://api.chucknorris.io/jokes/random").then((data) => {
        createCard(data?.value);
      });
    }, 3000);
  } else {
    clearInterval(timerID);
    timerID = null;
    spinner.classList.remove("spinner_visible");
  }
});

function addJokesToArray(arr, jokes) {
  if (!(arr.length < 10)) {
    arr.shift();
  }
  arrQuotes.push(jokes);
}

function addJokesToFavourite(event) {
  const target = event.target.parentNode;
  let quote = target.parentNode.nextElementSibling.textContent;
  if (!target.classList.contains("card__button-heart_active")) {
    target.classList.add("card__button-heart_active");
    addJokesToArray(arrQuotes, quote);
    localStorage.setItem("arrQuotes", JSON.stringify(arrQuotes));
  } else {
    target.classList.remove("card__button-heart_active");
    arrQuotes.splice(arrQuotes.indexOf(quote), 1);
    localStorage.setItem("arrQuotes", JSON.stringify(arrQuotes));
  }
}

function createCard(data) {
  const templateClone = template.content.cloneNode(true);
  templateClone.querySelector(".card__description").textContent = data;
  templateClone.querySelector(".card__button-heart path").addEventListener("click", addJokesToFavourite);
  jokesCard.append(templateClone);
}
