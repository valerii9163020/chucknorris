const cardsList = document.querySelector(".jokes__cards");

const clearFavorites = document.querySelector("#clearFavorites");
clearFavorites.addEventListener("click", () => {
  localStorage.clear();
  cardsList.innerHTML = "";
  disabledButton(clearFavorites);
});

const arrQuotesFromLocalstorage = localStorage.getItem("arrQuotes");
let arrQuotes = arrQuotesFromLocalstorage ? JSON.parse(arrQuotesFromLocalstorage) : [];

function checkedArrLength(arr) {
  if (arr.length === 0) {
    disabledButton(clearFavorites);
  } else {
    arr.map((item) => createCard("#favoriteTemplate", item));
  }
}

checkedArrLength(arrQuotes);

function createCard(selector, data) {
  const favoriteTemplateClone = document.querySelector(selector).content.cloneNode(true);
  favoriteTemplateClone.querySelector(".card__description").textContent = data;
  favoriteTemplateClone.querySelector(".card__button").addEventListener("click", deleteJokesToFavourite);
  cardsList.append(favoriteTemplateClone);
}

function deleteJokesToFavourite(event) {
  const target = event.target;
  const button = target.closest(".card__button");
  let quote = button.nextElementSibling.textContent;

  arrQuotes.splice(arrQuotes.indexOf(quote), 1);
  localStorage.setItem("arrQuotes", JSON.stringify(arrQuotes));
  cardsList.innerHTML = "";

  checkedArrLength(arrQuotes);
}

function disabledButton(button) {
  button.setAttribute("disabled", true);
}
